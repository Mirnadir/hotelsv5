$(document).ready(function() {
    // featured-section-games start
    $('.featured-section-games li:first-child').addClass('active');
    $('.button-item').hide();
    $('.button-item:first').show();

    $('.featured-section-games li').click(function(){
    $('.featured-section-games li').removeClass('active');
    $(this).addClass('active');
    $('.button-item').hide();
    
    var activeLink = $(this).find('a').attr('href');
    $(activeLink).fadeIn();
    return false;
    });
    // featured-section-games end

    // menu [[
    $('.ham-button img').click(function() {
        $('.mobileLinks').slideToggle(300);
    })
    // menu ]]
})
